# Project Awards by Codemotion
Visit our site on http://myproject.codemotion.com/

AGORA by Codemotion
is a website that lets developers share, discuss and vote on new and valuable ideas. Our main audience is software developers. Anyone visiting our site is encouraged to submit projects or ideas which then will be listed in a linear format by date. The site includes a comments system and a voting system similar to Reddit. The ideas and/or projects that get the most votes get funded either fully or partially by Codemotion.

How to use the site?
STEP 1: Create an account at the top right corner of the page.
STEP 2: Hit “SUBMIT” at the top right corner of the page.
STEP 3: Just like in JIRA, describe your idea in 1 sentence.
STEP 4: Give our readers your elevator-pitch in 200 characters.
STEP 5: Attach a direct link to the project from GitHub etc.
STEP 6: Pick a category you wish to place your project in.
STEP 7: Let us know if this idea is Yours or someone else’s.
STEP 8: Click SUBMIT and wait until our spam detectors clear your submission safe for view for all audiences.
STEP 9: Your project will be live on the homepage on the day it gets approved.
What happens after posting my project?
After posting your idea our users will take a close look at your project or code and CAST THEIR VOTE. A user may cast her/his vote by being logged in and pressing on the ARROW UP button next to the respective projects’ title.

What happens if I “win”?
If and when your project ranked the highest in the respective category or time frame (see terms and conditions), then your idea might get funded. The maximum amount of funding you may get differs per project.

Content Ordering
The submission content on the site can be ordered one of two ways:

By a calculated rating that depends on upvotes, date, and comments
By publish date
This is controlled on the options page using the “Use algorithm on homepage” setting. If this is set to “yes,” then the rating is used; if it is set to “no,” then the publish date is used and submissions are gathered into groups by the day they were submitted.

The rating algorithm is similar to Reddit’s:

agora_rating_algorithm
The number of upvotes a submission has is combined with the number of comments it has received to determine a “score.” This score is then combined with the publish date to determine its ranking. Older posts experience time decay of their rating, making it so that older posts with a high score will still fall off the front page.

A new rating is calculated when an upvote or downvote happens, when a comment is submitted, and hourly. The update is done in stages, calculating older submissions every other hour. This should not affect the load time of your site.

Read the full terms and conditions here.
ABOUT CODEMOTION
Codemotion.com and the Codemotion Software Developer Conference series

deliver high-quality content to over 350,000 software developers all across Europe each day. Derived from our Online and Offline events we offer companies a unique opportunity to engage with a well segmented, active and curious community of software developers and IT professionals.

The Codemotion Platform and the annual Conferences cover these tracks and interests on a multi-channel basis:

Devops, Blockchain, AI/Machine Learning, Inspirational, GameDev, IoT Big Data, Front-end, Cloud, Cybersecurity, Serverless, Software Architecture, AR/MR/VR, Design, UX.

We love what we do and we are very good at what we do. If you would like to work in our Team, please visit our career page for open positions.
